// @codekit-prepend "../../bower_components/jquery/dist/jquery.min.js";
// @codekit-prepend "../../bower_components/bootstrap/dist/js/bootstrap.min.js";
// @codekit-prepend "../../bower_components/slick.js/slick/slick.min.js";
// @codekit-prepend "../../bower_components/matchHeight/jquery.matchHeight.js";
// @codekit-prepend "../../bower_components/jquery.stickyFooter/dist/jquery.stickyFooter.js";
// @codekit-prepend "../../bower_components/jquery.columnizer/src/jquery.columnizer.min.js";
// @codekit-prepend "../../bower_components/jquery-cookie/jquery.cookie.js";
// @codekit-prepend "checkout.js";



// ! Document ready
$(document).ready(function() {

	// ! IE10 viewport hack for Surface/desktop Windows 8 bug
	(function() {
		'use strict';
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
			document.createTextNode('@-ms-viewport{width:auto!important}'))
			document.querySelector('head').appendChild(msViewportStyle)
		}
	})();
	

	
	// ! Touch detection + menu handling
	jQuery.support.touch = 'ontouchend' in document;

	if(jQuery.support.touch){
		$('.categorynav-section>ul>li').on('click', function() {
			$(this).toggleClass('open');
			$('.categorynav-section>ul>li').not(this).removeClass('open');
		});
	}
	else {
		$('.categorynav-section>ul>li').on('mouseenter mouseleave', function() {
			$(this).toggleClass('open');
		});
	}

	
	
	// ! Carousel
	$('.slider-section').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  adaptiveHeight: true,
	  autoplay: true,
	  prevArrow: '<button type="button" class="icon prev hidden-xs hidden-sm"></button>',
	  nextArrow: '<button type="button" class="icon next hidden-xs hidden-sm"></button>'
	});


	// ! Equal height
	$('.equalheight').matchHeight();
	$('.offer-text').matchHeight();
	
	
	

	// Populate modal	 + open modal
	$('.offer-image').on('click', function() {
				
		var $section = $(this).parents('.offercard');
		var $modal = $('#offerModal');
		
		// Get values from section
		var $header = $section.find('.description span').text();
		var $image = $section.find('.offer-image-container div').css("background-image");
		var $text = $section.find('.modal-description').html();
		
		// Set values im Modal
		$modal.find('.modal-title').empty().prepend($header);
		$modal.find('.modal-offer-image div').css('background-image', $image);
		$modal.find('.modal-offer-description').empty().prepend($text);
	
		
		$modal.modal('show');
		
		return false;
	});

	// Add to cart	
	$('.addtocart').on('click', function() {
		$('#cartModal').modal('show');
	});

	// Category columns
	$('.megamenu').columnize({ width: 180 });
	
	
	// Mypage contact
	$('.edit-contact').on('click', function() {
		var $target = $(this).parents('.myinfo').find('.mypage-contact');
		$target.collapse('toggle');
		if($(this).html() == 'Ändra') {
			$(this).empty().prepend('Avbryt');
		}
		else {
			$(this).empty().prepend('Ändra');
		}
		return false;
	});
	$('.save-contact').on('click', function() {
		var $target = $(this).parents('.myinfo').find('.mypage-contact');
		$target.collapse('toggle');
	});
	
	// Cookie alert
	$('.cookie-alert').on('click', 'button', function() {
		$('.cookie-alert').addClass('collapse');
		$.cookie('eu-cookies-conscent', 'true', { expires: 365, path: '/' });
	});
	if($.cookie('eu-cookies-conscent') == "true") {
		$(".cookie-alert").css("display", "none");
	};





	// Dev (to be removed)
/*
	$( "body" ).prepend( "<div id='browser' class='closed'><a href='https://bitbucket.org/likabra/dintidning2/issues'>Issuetracker</a><br><a href='getzip.php'>Download zip</a><br><br><a href='index.html'>Start</a><br><a href='category.html'>Category</a><br><a href='product.html'>Product</a><br><a href='product-product.html'>Product-product</a><br><a href='product-product-2.html'>Product-product 2</a><br><a href='checkout-standard.html'>Checkout-standard</a><br><a href='checkout-quick.html'>Checkout-quick</a><br><a href='checkout-quick-product.html'>Checkout-quick prod</a><br><a href='checkout-campaign.html'>Checkout-campaign</a><br><a href='checkout-campaign-v2.html'>Checkout-campaign 2</a><br><a href='checkout-empty.html'>Checkout-empty</a><br><a href='reciept.html'>Reciept</a><br><a href='login.html'>Login</a><br><a href='mypage.html'>Mina sidor</a><br><a href='search.html'>Sökresultat</a><br><a href='standard.html'>Standardsida</a><br><a href='backissues.html'>Backissues</a><br></div>" );
	$('#browser').on('click', function() {
		$(this).toggleClass('closed');
	});
*/



});


// ! Sticky footer
$(window).load(function() {
    $("#footer").stickyFooter();
});




