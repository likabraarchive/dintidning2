// ! Document ready
$(document).ready(function() {

	// ! Checkout - selected offer
	$('.upsell').on('click', function() {
		$(this).addClass('selected');
		$(this).find('input').prop("checked", true);
		$('.upsell').not(this).removeClass('selected');
	});	



	// Annan mottagare - checkbox
	$('.checkout-shipto .checkbox input').change(function() {
		var $section = $(this).parents('.checkout-shipto');
		if(this.checked) {
			$section.find('.shipto-form').collapse('show');
			$section.find('.shipto-saved').css('margin-top', '10px')
		}
		else {
			shiptoReset($section);			
		}
	});
	
	
	// Ångra
	$('.shipto-cancel').on('click', function() {
			var $section = $(this).parents('.checkout-shipto');
			$section.find('.shipto-form').collapse('hide');
			$section.find('.shipto-saved').css('margin-top', '0px');
			$section.find('.checkbox input').prop('checked', false);
	});
	
	
	// Spara
	$('.shipto-save').on('click', function() {
		
		var $section = $(this).parents('.checkout-shipto');
		
		shiptoValidation($section);
		
		if(shiptoValidation($section) == 'valid') {

			var $name = $section.find( ".shipto-form .fname" ).val() + ' ' + $section.find( ".shipto-form .ename" ).val();
			var $street = $section.find( ".shipto-form .street" ).val();
			$section.find( ".shipto-saved .name" ).prepend( $name );			
			$section.find( ".shipto-saved .street" ).prepend( $street );
			$section.find('.shipto-form').collapse('hide');
			$section.find('.shipto-saved').css('margin-top', '5px');
			$section.find('.shipto-edit').css('display', 'block');
		}
		
	});
	
	
	// Ändra
	$('.shipto-edit').on('click', function() {

		var $section = $(this).parents('.checkout-shipto');		
		
		$section.find('.shipto-form').collapse('show');
		$section.find('.shipto-saved').css('margin-top', '10px');
		$(this).css('display', 'none');
		$section.find('.shipto-saved .name').empty();
		$section.find('.shipto-saved .street').empty();
	});

	
	// Validation
	function shiptoValidation($this) {
		
		var $this;				
		
		if(
			$this.find('.shipto-form .fname').val() && 
			$this.find('.shipto-form .ename').val() && 
			$this.find('.shipto-form .street').val() && 
			$this.find('.shipto-form .zip').val() && 
			$this.find('.shipto-form .city').val()
		) 
		{
			$this.find('.shipto-form .form-control').removeClass('error');
			return("valid");
		}
		else {
			$this.find('.shipto-form .form-control').not('.shipto-form .form-control.company').filter(function() {
					return !this.value;
				}).addClass("error");​
		}
	}
	
	// Reset form
	function shiptoReset($this) {

		var $this;				

		$this.find('.shipto-form').collapse('hide');
		$this.find('.shipto-saved').css('margin-top', '0px');
		$this.find('.shipto-form input').val('');
		$this.find('.shipto-form select').val('se');
		$this.find('.checkout-shipto .checkbox input').prop('checked', false);
		$this.find('.shipto-form .form-control').removeClass('error');
		$this.find('.shipto-saved .name').empty();
		$this.find('.shipto-saved .street').empty();
		$this.find('.shipto-edit').css('display', 'none');
	}




	// Populate modal	 + open modal
	$('.thumbnail-offer').on('click', function() {
				
		var $section = $(this).parents('.checkout-product');
		var $modal = $('#checkoutModal');
		
		// Get values from section
		var $image = $(this).css("background-image");
		var $text = "";
		
		// Set values im Modal
		$modal.find('.modal-offer-image div').css('background-image', $image);
		$modal.find('.modal-offer-description').empty().prepend($text);
	
		
		$modal.modal('show');
		
		return false;
	});
	
	

});

